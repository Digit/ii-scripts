ii-scripts
==========

Digit's various ii scripts


haz:
====

biip
with some configuration and a sound file, can be used as audio notification bell for nickpings or other key words.

perpetualii
simple persistant input to a chan/pm/server/bot, to spare you repeatedly typing echo etc.

hiir
channel output


more about:
===========

use with suckless's irc client called "ii".  https://tools.suckless.org/ii/

hiir and perpetualii work together well, in tmux/screen/dvtm, or some other means of splitting.